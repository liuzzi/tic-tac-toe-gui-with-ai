Author: Francesco Liuzzi
--------------------------------------------------------------------------
Description:
	Tic Tac Toe game with a Tkinter GUI and an	
Artificial intelligence agent that never loses.




This application uses the python Tkinter library for its gui.  
(this is the python-tk package in ubuntu) -- sudo apt-get install python-tk

Other than that, it should simply be able to be run by:

python AITicTacToe.py
