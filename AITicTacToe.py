#!/usr/bin/env python
#Francesco Liuzzi
#Artificial Intelligence
#Search Programming Assignment
#
#Acknowledgements:
#skeleton minimax code and tictactoe board layout based on 
#sarath lakshman's own implmentation (see doc for more info)

from Tkinter import *
from ttk import Frame, Button, Label, Style
from ttk import Entry
import random

class TICTACTOE:
    def __init__(self,player1,player2):
        #board will be represented by a simple array of 9 positions, started with None values
        self.board = [ None for i in range(0,9) ]

        #last moves stack
        self.lastmoves = []

        #member variables
        self.winner = None
        self.p1 = player1
        self.p2 = player2

    def get_free_moves(self):
        moves = []
        
        #iterate the board and look for None values (free positions)
        for index,value in enumerate(self.board):
            if value==None:
                moves.append(index)
        return moves

	#mark the board at the position with the given player marker. (change array val at index)
	#Also, add move to lastmoves stack
    def mark(self,marker,position):
        self.board[position] = marker
        self.lastmoves.append(position)

    def revert_last_move(self):
	#pop off the last value, and make sure the winner isnt still set (from minimax analysis)
        self.board[self.lastmoves.pop()] = None
        self.winner = None

    def is_gameover(self):
        #all of the possible winning positions on a 9x9 board.
        win_positions = [(0,1,2), (3,4,5), 
                         (6,7,8), (0,3,6),
                         (1,4,7),(2,5,8), 
                         (0,4,8), (2,4,6)]

	#iterate through the triplets
        for i,j,k in win_positions:
	    #check for equality of markers and return result
            if self.board[i] == self.board[j] == self.board[k] and self.board[i] != None:
                self.winner = self.board[i]
                return True

	#check for the draw (No winner found and no more free spaces.)
        if None not in self.board:
            self.winner = None
            return True

        return False

#HUMAN class: just used to hold a marker
class Human:
    def __init__(self,marker):
        self.marker = marker
         
class AI:
    def __init__(self, marker, opponentmarker):
        self.marker = marker
	self.opponentmarker = opponentmarker

	#when AI makes a move, we want to maximize
    def move(self,gameinstance):
        move_position,score = self.maximized_move(gameinstance)
        return move_position

    #MAX function:  find the max move
    def maximized_move(self,game):
        bestscore, bestmove = None, None

        #iterate every free position left
        for freeposition in game.get_free_moves():

	#mark the free space with the self(AI) marker since max
            game.mark(self.marker,freeposition)
        
            if game.is_gameover():
                #reached where the game will end, calculate score
                score = self.get_score(game)
            else:
		#if game not ended, evaluate min's move
                move_position,score = self.minimized_move(game)
        
            #revert the move (to return the board to what it was before minimax recursion)
            game.revert_last_move()
            
            #set max score
            if bestscore == None or score > bestscore:
                bestscore = score
                bestmove = freeposition

        return bestmove, bestscore

    #MIN function: find the min move
    def minimized_move(self,gameinstance):
        bestscore, bestmove = None, None

	#iterate every free position left
        for freeposition in gameinstance.get_free_moves():

	#mark the free space with the opponent(HUMAN) marker since min
            gameinstance.mark(self.opponentmarker,freeposition)
        
            if gameinstance.is_gameover():
		#reached where the game will end, calculate score
                score = self.get_score(gameinstance)
            else:
		#if game not ended, evaluate max's move
                move_position,score = self.maximized_move(gameinstance)
        
	    #revert the move (to return the board to what it was before minimax recursion)
            gameinstance.revert_last_move()
            
	    #set max score
            if bestscore == None or score < bestscore:
                bestscore = score
                bestmove = freeposition

        return bestmove, bestscore

    #evaluate the winner of the given instance
    def get_score(self,gameinstance):
        if gameinstance.is_gameover():
            if gameinstance.winner  == self.marker:
                return 1 # Won
            elif gameinstance.winner == self.opponentmarker:
                return -1 # Opponent won
        return 0 # Draw


class GUI(Frame):
    def newgame(self):
        #reset buttons
        self.one["text"] = ""
        self.two["text"] = ""
        self.three["text"] = ""
        self.four["text"] = ""
        self.five["text"] = ""
        self.six["text"] = ""
        self.seven["text"] = ""
        self.eight["text"] = ""
        self.nine["text"] = ""

        #reset entry
        self.entry.delete(0,25)
        self.entry.insert(0, "Pick a Move!")
   
        #reassign players
        rand = random.randint(0,1)
        
        if rand == 0:
            player1 = Human("X")
            player2 = AI("O","X")
        else:
            player1 = Human("O")
            player2 = AI("X","O")
            
        self.game = TICTACTOE(player1, player2)

        if self.game.p1.marker == "O":
            self.computer_moves_first()
        
    def mark(self, button, index):
        if button["text"] == "":
            if not self.game.is_gameover():
                button["text"] = self.game.p1.marker
                self.game.mark(self.game.p1.marker,index-1)
                if not self.check_end_game():
                    move_position = self.game.p2.move(self.game)
                    self.game.mark(self.game.p2.marker,move_position)
                    self.mark_button_by_index(move_position)
                    self.check_end_game()

    def check_end_game(self):
        if self.game.is_gameover():
            #increment and update game count
            self.increment_and_update_game_count()

            if self.game.winner == None:
                self.entry.delete(0,25)
                self.entry.insert(0,"DRAW!")                        
            else:
                self.entry.delete(0,25)
                self.entry.insert(0,"COMPUTER WINS!")
                self.ai_score += 1
                self.display_scores()
            return True
        return False

    def mark_button_by_index(self, i):
        if i == 0:
            self.one["text"] = self.game.p2.marker
        elif i == 1:
            self.two["text"] = self.game.p2.marker
        elif i == 2:
            self.three["text"] = self.game.p2.marker
        elif i == 3:
            self.four["text"] = self.game.p2.marker
        elif i == 4:
            self.five["text"] = self.game.p2.marker
        elif i == 5:
            self.six["text"] = self.game.p2.marker
        elif i == 6:
            self.seven["text"] = self.game.p2.marker
        elif i == 7:
            self.eight["text"] = self.game.p2.marker
        elif i == 8:
            self.nine["text"] = self.game.p2.marker
            
    def __init__(self, parent, game):
        Frame.__init__(self, parent)
        self.parent = parent
        self.game = game

        self.parent.title("Tic Tac Toe - F. Liuzzi")
        
        Style().configure("TButton", padding=(0, 20, 0, 20), 
                          font='serif 10')

        self.one = Button(self, text="")
        self.one.config(command=lambda button=self.one:
                       self.mark(button,1))
        self.one.grid(row=1, column=0)


        self.two = Button(self, text="")
        self.two.config(command=lambda button=self.two:
                       self.mark(button,2))
        self.two.grid(row=1, column=1)
        
        self.three = Button(self, text="")
        self.three.config(command=lambda button=self.three:
                         self.mark(button,3))
        self.three.grid(row=1, column=2)

        self.four = Button(self, text="")
        self.four.config(command=lambda button=self.four:
                        self.mark(button,4))
        self.four.grid(row=2, column=0)
        
        self.five = Button(self, text="")
        self.five.config(command=lambda button=self.five:
                        self.mark(button,5))
        self.five.grid(row=2, column=1)
        
        self.six = Button(self, text="")
        self.six.config(command=lambda button=self.six:
                       self.mark(button,6))
        self.six.grid(row=2, column=2)

        self.seven = Button(self, text="")
        self.seven.config(command=lambda button=self.seven:
                         self.mark(button,7))
        self.seven.grid(row=3, column=0)

        self.eight = Button(self, text="")
        self.eight.config(command=lambda button=self.eight:
                         self.mark(button,8))
        self.eight.grid(row=3, column=1)

        self.nine = Button(self, text="")
        self.nine.config(command=lambda button=self.nine:
                        self.mark(button,9))
        self.nine.grid(row=3, column=2)
        
        self.columnconfigure(0, pad=3)
        self.columnconfigure(1, pad=3)
        self.columnconfigure(2, pad=3)
        
        self.rowconfigure(0, pad=3)
        self.rowconfigure(1, pad=3)
        self.rowconfigure(2, pad=3)
        self.rowconfigure(3, pad=3)
        self.rowconfigure(4, pad=3)
        self.rowconfigure(5, pad=30)
        self.rowconfigure(6,pad=3)
        self.rowconfigure(7, pad=0)

        self.newgame = Button(self, text="New Game",command=self.newgame)
        self.newgame.config(padding=(0, 10, 0, 10))
        self.newgame.grid(row=5,columnspan=3, sticky=W+E)

        self.entry = Entry(self)
        self.entry.grid(row=0, columnspan=3, sticky=W+E)
        self.entry.insert(0,"Pick a Move!")
        self.entry.config(justify=CENTER)

        #set up scores
        self.human_score = 0
        self.ai_score = 0

        self.scores = Entry(self)
        self.scores.grid(row=6, columnspan=3, sticky=W+E)
        self.scores.config(justify=CENTER)
        self.display_scores()

        
        self.games_played = 0
        self.game_counter = Entry(self)
        self.game_counter.grid(row=7, columnspan=3, sticky=W+E)
        self.game_counter.config(justify=CENTER)
        self.game_counter.insert(0,"Games Played: 0")

        self.pack()

        if self.game.p1.marker == "O":
            self.computer_moves_first()

    def display_scores(self):
        str = "Player: {0}        Computer: {1}".format(
            self.human_score,self.ai_score)
        self.scores.delete(0,40)
        self.scores.insert(0,str)

    def increment_and_update_game_count(self):
        self.games_played += 1
        str = "Games Played: {0}".format(self.games_played)
        self.game_counter.delete(0,40)
        self.game_counter.insert(0,str)
        

    def computer_moves_first(self):
        first_rand_move = random.randint(0,8)
        self.game.mark(self.game.p2.marker,first_rand_move)
        self.mark_button_by_index(first_rand_move)


if __name__ == '__main__':
     #decide randomly who goes first
    rand = random.randint(0,1)
    
    if rand == 0:
        player1 = Human("X")
        player2 = AI("O","X")
    else:
        player1 = Human("O")
        player2 = AI("X","O")
    
    game = TICTACTOE(player1, player2)
    root = Tk()
    app = GUI(root,game)
    root.mainloop()
